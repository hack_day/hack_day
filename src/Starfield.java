import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Andrew Meads on 9/01/2018.
 */
public class Starfield {

    private List<Star> stars;
    Random rnd;

    public Starfield(int width, int height) {

        stars = new ArrayList<>();
        rnd = new Random();

        for (int i = 0; i < 100; i++) {
            int rndHeight = rnd.nextInt(height);
            int rndWidth = rnd.nextInt(width);
            int rndSize = rnd.nextInt(4) + 3; //between 2 and 6 (inc)
            int rndSpeed = rnd.nextInt(6) + 6; //between 5 and 11 (inc)
            stars.add(new Star(rndWidth, rndHeight, rndSize, rndSpeed));
        }

    }

    public void move(int windowWidth, int windowHeight) {

        for (int i = 0; i < stars.size(); i++) {
            stars.get(i).move(windowWidth, windowHeight);
        }
    }


    public void paint(Graphics g) {

        for (int i = 0; i < stars.size(); i++) {
            stars.get(i).paint(g);
        }
    }

}
