import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by Andrew Meads on 9/01/2018.
 */
public class Asteroid {

    private static final int SPRITE_WIDTH = 32, SPRITE_HEIGHT = 32;

    private int x, y, width, height, speed;

    private Direction direction;

    private Image image;


    public Asteroid(int x, int y, int speed) {

        direction = Direction.Up;

        try {
            this.image = ImageIO.read(new File("asteroid.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.x = x;
        this.y = y;
        this.width = SPRITE_WIDTH;
        this.height = SPRITE_HEIGHT;
        this.speed = speed;


    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void move(Direction direction, int windowWidth, int windowHeight) {

        y = y + speed;
        if (y > windowHeight)
            y = 0;

    }

    public void paint(Graphics g) {



        if (this.direction == Direction.Up){
            drawShip(g, 0, 0);
            return;
        }

        if (this.direction.equals(Direction.Down)){
            drawShip(g, 0, 0);
            return;
        }

        if (this.direction == Direction.Left){
            drawShip(g, 0, 0);
            return;
        }

        if (this.direction == Direction.Right){
            drawShip(g, 0, 0);
            return;
        }



    }

    private void drawShip(Graphics g, int sx1, int sy1) {
        g.drawImage(image, x, y, x + width, y + height, sx1, sy1, width+sx1, height+sy1, null);
    }

}
