import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Explosion {

    private static final int SPRITE_WIDTH = 128, SPRITE_HEIGHT = 128;

    private int x, y, width, height;

    private Direction direction;

    private Image image;

    public boolean explosionEnabled = false;


    public Explosion(int x, int y) {

        direction = Direction.Up;

        try {
            this.image = ImageIO.read(new File("explosion.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.x = x;
        this.y = y;
        this.width = SPRITE_WIDTH;
        this.height = SPRITE_HEIGHT;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void paint(Graphics g) {

        drawExplosion(g);

    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void drawExplosion(Graphics g) {

        if (explosionEnabled)
            for (int i = 0; i < 896; i += 128)
                for (int j = 0; j < 896; j += 128){
//                    try {
//                        TimeUnit.MILLISECONDS.sleep(200);
                        g.drawImage(image, x, y, x + width, y + height, j, i, width + j, height + i, null);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }

        }

    }

//    private void drawExplosion(Graphics g, int sx1, int sy1) {
//        g.drawImage(image, x, y, x + width, y + height, sx1, sy1, width + sx1, height + sy1, null);
//    }

}
